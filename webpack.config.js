module.exports = {
  entry: ["babel-polyfill", "./js/app.js"],
  output: {
    filename: "./build/app.js",
    library: 'erastotenes'
  },
  resolve: {
    extensions: ['.js']
  },
  module: {
    rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    }
    ]
  }
}
