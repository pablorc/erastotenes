const TiledLayerFacade = function(layer) {
  const self = new Object();

  self.hide = () => layer.set('hidden', true);

  self.show = () => layer.set('hidden', false);

  return self;
};

module.exports = TiledLayerFacade;
