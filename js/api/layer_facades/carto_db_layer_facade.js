const TiledLayerFacade = require('./tiled_layer_facade');

const CartoDBLayerFacade = function(layer, urlTemplateResolver) {
  const self = new Object(new TiledLayerFacade(layer));

  self.setSql = async (sql) => {
    const newAttributes = Object.assign({}, layer.attributes, { sql });
    const urlTemplate = await urlTemplateResolver.resolve(newAttributes);

    layer.set('urlTemplate', urlTemplate);
  }

  return self;
};

module.exports = CartoDBLayerFacade;
