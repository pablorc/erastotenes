const TiledLayerFacade = require('./layer_facades/tiled_layer_facade');
const CartoDBLayerFacade = require('./layer_facades/carto_db_layer_facade');

const capitalizer = require('../utils/capitalizer');

const LayerFacadeFactory = function(layer, config) {
  const self = new Object();

  /* Builds a Layer object from its specification */
  self.build = function() {
    const f = `${capitalizer(layer.get('type'))}LayerFacade`;
    const klass = eval(f);

    return klass(layer, config);
  }

  return self;
}

module.exports = LayerFacadeFactory;



