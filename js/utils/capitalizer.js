/* Capitalizes a string */
const capitalizer = (string) => string.charAt(0).toUpperCase() + string.substring(1);

module.exports = capitalizer;

