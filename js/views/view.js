const Backbone = require('backbone');
const _ = require('underscore');

const View = Backbone.View.extend({
  initialize(attrs) {
    this.center = attrs.center;
    this.zoom = attrs.zoom;
    this.layers = attrs.layers;
    this.domId = attrs.domId;
    this.mapAdapter = attrs.mapAdapter;

    this.layers.forEach((layer) => this.listenTo(layer, 'change', this.renderLayers));
  },

  createLayer(layer) {
      const urlTemplate = layer.get('urlTemplate');
      const options = _.pick(layer.attributes, 'max_zoom', 'min_zoom', 'attribution');

      return this.mapAdapter.createLayer(urlTemplate, options);
  },

  clearLayers() {
    if(!this.map) {
      return;
    }

    this.map.eachLayer((layer) => this.map.removeLayer(layer));
  },

  createLayers() {
    this.clearLayers();

    return this.layers
      .filter((layer) => !layer.get('hidden'))
      .map((layer) => this.createLayer(layer));
  },

  renderLayers() {
    this.createLayers()
      .forEach((layer) => this.mapAdapter.addLayerToMap(layer, this.map));
  },

  render() {
    this.map = this.mapAdapter.createMap(this.domId, this.center, this.zoom);

    this.renderLayers();
  }
});

module.exports = View;
