const L = require('leaflet');

const LeafletAdapter = function() {
  const createMap = function(domId, center, zoom) {
    return L.map(domId).setView(center, zoom);
  };

  const createLayer = function(urlTemplate, options) {
    return L.tileLayer(urlTemplate, options);
  };

  const addLayerToMap = function(layer, map) {
    layer.addTo(map);
  };

  return {
    createMap,
    createLayer,
    addLayerToMap
  }
};

module.exports = LeafletAdapter;
