const capitalizer = require('../utils/capitalizer');
const CartoDBUrlTemplateResolver = require('./carto_db_url_template_resolver');
const Layer = require('./layer');

const LayerFactory = function(layers, urlTemplateResolver) {

  const self = new Object();

  /* Creates a Layer object from a tiled specification */
  const buildTiledLayer = function(layer) {
    return new Layer(layer);
  }

  /* Creates a Layer object from a CartoDB specification. Returns a promise */
  const buildCartoDBLayer = async function(layer) {
    layer.urlTemplate = await urlTemplateResolver.resolve(layer);
    return new Layer(layer);
  };

  /* Builds a Layer object from its specification */
  self.build = async function(layer) {
    const f = `build${capitalizer(layer.type)}Layer`;

    layer.options.type = layer.type;
    return eval(f)(layer.options);
  }

  return self;
}

module.exports = LayerFactory;



