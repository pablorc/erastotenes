const CartoDBUrlTemplateResolver = function(config) {
  const self = new Object();

  /* Generates payload for the API call that returns the layerGroupId */
  const buildPayload = function(layer) {
    return {
      "layers": [{
        "type": "mapnik",
        "options": {
          ...layer,
          "interactivity": [
            "cartodb_id"
          ]
        }
      }]
    }
  };

  /* Generates the url to get the layerGroupId */
  const urlEndpoint = function() {
    const baseUrlEndpoint = config.maps_api_template.replace(/{user}/, config.user_name);
    return `${baseUrlEndpoint}/api/v1/map`;
  }

  /* Returns the urlTemplate to get tiles for a CartoDB layer */
  self.resolve = async function(layer) {
    try {
    const headers = new Headers({
      "Content-Type": "application/json"
    });

    const response = await fetch(urlEndpoint(), {
      method: 'POST',
      body: JSON.stringify(buildPayload(layer)),
      headers
    });

    const json = await response.json();
    const layergroupid = json.layergroupid;
    return `http://ashbu.cartocdn.com/${config.user_name}/api/v1/map/${layergroupid}/0/{z}/{x}/{y}.png`;
    } catch (err) {
      console.log(err);
      throw 'Error resolving url template';
    }
  }

  return self;
};

module.exports = CartoDBUrlTemplateResolver;
