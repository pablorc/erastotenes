require("babel-core/register");
require("babel-polyfill");

const LayerFactory = require('./core/layer_factory');
const LayerFacadeFactory = require('./api/layer_facade_factory');
const CartoDBUrlTemplateResolver = require('./core/carto_db_url_template_resolver');

const LeafletAdapter = require('./views/map_adapters/leaflet_adapter');
const View = require('./views/view');

/* Generates the desired map, given the id of the element to print on, and a map specification */
var erastotenes = function(domId, options) {
  const resolver = CartoDBUrlTemplateResolver(options.maps_api_config);
  const layerFactory = LayerFactory(options.layers, resolver);
  const promisedLayers = options.layers.map((layer) => layerFactory.build(layer));

  Promise.all(promisedLayers).then((layers) => {
    const view = new View({
      center: eval(options.center),
      zoom: options.zoom,
      map,
      layers,
      domId,
      mapAdapter: LeafletAdapter()
    })
    .render();
  });

  return Promise.all(promisedLayers)
    .then((layers) => layers.map((layer) => new LayerFacadeFactory(layer, resolver).build()))
};

module.exports = erastotenes;
