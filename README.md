# erastotenes #

A simple mapping library.

### Build ###

Requisites: `npm`

1. Clone the repository
1. `npm install`
1. `npm run build`

### Usage ###

* Once the setup is done, open the `index.html` file from your browser.

### API ###

The API is quite simple.

All the functionality is exposed within a single function: `erastotenes(domID, layers)`. The arguments are:

1. **domId**: The id of the element where the map should be rendered
1. **layers**: The specification of layers, following the structure from CARTO's engine test exercise.

The function returns a promise that will be resolved into a array of layers. The order is the same as the specification given.

Each layer object contains the actions allowed for that object:

* `hide()`: Hides the layer. Available for all layers.
* `show()`: Shows the layer. Available for all layers.
* `setSql(aSql)`: Changes the SQL that populates the layer. Available only for layers of type _CartoDB_.

### Notes ###

It's missing an important part for the API. Right now Leaflet styles are loaded inside the index.html file. This should be dinamically added from the view to not break the API if the mapping library changes. Due to a lack of time I'm not fixing it, but I wanted to remark how it should be done.
